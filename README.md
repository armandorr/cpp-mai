# CPP laboratories

Constraint Processing and Programming (CPP) - Master in Artificial Intelligence

- Basic Modeling for Discrete Optimization course: https://www.coursera.org/learn/basic-modeling
- Advanced Modeling for Discrete Optimization course: https://www.coursera.org/learn/advanced-modeling

## Author
- Armando Rodriguez Ramos